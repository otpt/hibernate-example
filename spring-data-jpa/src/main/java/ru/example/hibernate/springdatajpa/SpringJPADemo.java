package ru.example.hibernate.springdatajpa;

import java.util.List;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.example.hibernate.springdatajpa.config.DataJpaConfig;
import ru.example.hibernate.springdatajpa.entities.Album;
import ru.example.hibernate.springdatajpa.entities.Singer;
import org.springframework.context.support.GenericXmlApplicationContext;

public class SpringJPADemo {
    public static void main(String... args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(DataJpaConfig.class);

        SingerService singerService = ctx.getBean(
            "springJpaSingerService", SingerService.class);

        listSingers("Find all:", singerService.findAll());
        listSingers("Find by first name:", singerService.findByFirstName("John"));
        listSingers("Find by first and last name:",   
            singerService.findByFirstNameAndLastName("John", "Mayer"));
        Singer singer = singerService.findById(1L);
        Album albums = singer.getAlbums().iterator().next(); // detached LIEx
    }

    private static void listSingers(String message, List<Singer> singers) {
        System.out.println("");
        System.out.println(message);
        for (Singer singer: singers) {
            System.out.println(singer);
            System.out.println();
        }
    }

}
