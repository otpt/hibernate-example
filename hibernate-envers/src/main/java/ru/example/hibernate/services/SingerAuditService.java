package ru.example.hibernate.services;

import ru.example.hibernate.entities.Singet;

import java.util.List;

public interface SingerAuditService {
    List<Singet> findAll();
    Singet findById(Long id);
    Singet save(Singet singet);
    Singet findAuditByRevision(Long id, int revision);
}
