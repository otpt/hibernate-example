package ru.example.hibernate.services;

import ru.example.hibernate.entities.Singet;
import ru.example.hibernate.repos.SingerAuditRepository;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service("singerAuditService")
@Transactional
public class SingerAuditServiceImpl implements SingerAuditService {

	@Autowired
	private SingerAuditRepository singerAuditRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(readOnly = true)
	public List<Singet> findAll() {
		return singerAuditRepository.findAll();
	}

	/**
	 * API  changed in  2.0.0.M3 findOne became findById
	 * @param id
	 * @return
	 */
	public Singet findById(Long id) {
		return singerAuditRepository.findById(id).get();
	}

	public Singet save(Singet singer) {
		return singerAuditRepository.save(singer);
	}

	@Transactional(readOnly = true)
	@Override
	public Singet findAuditByRevision(Long id, int revision) {
		AuditReader auditReader = AuditReaderFactory.get(entityManager);
		return auditReader.find(Singet.class, id, revision);
	}
}
