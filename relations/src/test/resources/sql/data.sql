insert into client (id, phone, created_date, last_modified_date, version)
values (100, '88005553534', '2011-01-01 00:00:00'::TIMESTAMP, '2011-01-01 00:00:00'::TIMESTAMP, 1);

insert into qr (id, qr_code, client_id, created_date, last_modified_date, version)
values (1, 'ы', 100, '2011-01-01 00:00:00'::TIMESTAMP, '2011-01-01 00:00:00'::TIMESTAMP, 1);
insert into parsed_qr (id, data, qr_id, client_id, created_date, last_modified_date, version)
values (1, 2, 1, 100, '2011-01-01 00:00:00'::TIMESTAMP, '2011-01-01 00:00:00'::TIMESTAMP, 1);

insert into position (id, position_name, parsed_qr_id, created_date, last_modified_date, version)
values (100, 's', 1,  '2011-01-01 00:00:00'::TIMESTAMP, '2011-01-01 00:00:00'::TIMESTAMP, 1);

insert into position (id, position_name, parsed_qr_id, created_date, last_modified_date, version)
values (200, 's', 1,  '2011-01-01 00:00:00'::TIMESTAMP, '2011-01-01 00:00:00'::TIMESTAMP, 1);

insert into position (id, position_name, parsed_qr_id, created_date, last_modified_date, version)
values (300, 's', 1,  '2011-01-01 00:00:00'::TIMESTAMP, '2011-01-01 00:00:00'::TIMESTAMP, 1);

