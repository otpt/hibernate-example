package ru.example.hibernate.relations.entity;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.util.Assert;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "parsed_qr")
@javax.persistence.Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ParsedQR extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "parsed_qr_id_seq")
    @SequenceGenerator(name = "parsed_qr_id_seq", sequenceName = "parsed_qr_id_seq", allocationSize = 1)
    private Long id;

    private Long data;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "qr_id", nullable = false)
    private QR qr;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;

    @OneToMany(mappedBy = "parsedQR", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Position> positions = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public QR getQr() {
        return qr;
    }

    public void setQr(QR qr) {
        Assert.isTrue(qr != null, "always must have qr");
        qr.setParsedQR(this);
        this.qr = qr;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<Position> getPositions() {
        return positions;
    }

    void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public void addOfferItem(Position position) {
        position.setParsedQR(this);
        positions.add(position);
    }

    public void removeOfferItem(Position position) {
        position.setParsedQR(null);
        positions.remove(position);
    }

    public Long getData() {
        return data;
    }

    public void setData(Long data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParsedQR parsedQR = (ParsedQR) o;
        return Objects.equals(getId(), parsedQR.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
