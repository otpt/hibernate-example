package ru.example.hibernate.relations.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OtherService {

    @Transactional()
    public void doSomething() {
        throw new RuntimeException();
    }
}
