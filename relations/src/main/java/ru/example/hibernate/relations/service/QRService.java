package ru.example.hibernate.relations.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.example.hibernate.relations.entity.Position;
import ru.example.hibernate.relations.entity.ParsedQR;
import ru.example.hibernate.relations.entity.QR;
import ru.example.hibernate.relations.repository.PositionRepository;
import ru.example.hibernate.relations.repository.QRRepository;
import ru.example.hibernate.relations.repository.ParsedQRRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class QRService {
    private final QRRepository QRRepository;
    private final ParsedQRRepository parsedQRRepository;
    private final PositionRepository positionRepository;

    @Autowired
    public QRService(QRRepository QRRepository, ParsedQRRepository parsedQRRepository, PositionRepository positionRepository) {
        this.QRRepository = QRRepository;
        this.parsedQRRepository = parsedQRRepository;
        this.positionRepository = positionRepository;
    }

    public void showEntityGraphQuery() {
        QRRepository.findAllByProcessedDateIsNull();
    }

    public void showJPQLQuery() {
        PageRequest pageRequest = PageRequest.of(1, 1);
        QRRepository.findAllByClient(1L, pageRequest);
    }


    public void wrongPaginationOverFetch() {
        PageRequest pageRequest = PageRequest.of(1, 1);
        parsedQRRepository.findAllByClientId(1L, pageRequest);
    }

    public void rightPaginationOverFetch() {
        PageRequest pageRequest = PageRequest.of(1, 1);
        List<ParsedQR> parsedQRS = parsedQRRepository.findAllByClient(1L, pageRequest);
        List<Position> positions = positionRepository.findAllByParsedQRIdIn(parsedQRS.stream().map(ParsedQR::getId).collect(Collectors.toList()));
        // todo merge qrs and positions
    }

    @Transactional
    public void editImmutable() {
        QR QR = QRRepository.findById(1L).get();
        QR.setQrCode("3444");
        QRRepository.save(QR);
    }

    @Transactional(readOnly = true)
    public void showProjection() {
        QRRepository.findByClientId(100L);
    }
}
