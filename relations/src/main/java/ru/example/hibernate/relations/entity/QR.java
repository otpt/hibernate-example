package ru.example.hibernate.relations.entity;

import org.hibernate.annotations.Immutable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "qr")
@Immutable
public class QR extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "qr_id_seq")
    @SequenceGenerator(name = "qr_id_seq", sequenceName = "qr_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "qr_code", nullable = false, unique = true)
    private String qrCode;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Client client;

    @Column(name = "processed_date")
    private Instant processedDate;

    @OneToOne(mappedBy = "qr", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ParsedQR parsedQR;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }



    public Instant getProcessedDate() {
        return processedDate;
    }

    public ParsedQR getParsedQR() {
        return parsedQR;
    }

    void setParsedQR(ParsedQR parsedQR) {
        this.parsedQR = parsedQR;
    }

    public void setProcessedDate(Instant processedDate) {
        this.processedDate = processedDate;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
