package ru.example.hibernate.relations.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ru.example.hibernate.relations.entity.QR;
import ru.example.hibernate.relations.entity.QRPart;

import java.util.List;

public interface QRRepository extends JpaRepository<QR, Long> {
    @EntityGraph(attributePaths = "client")
    List<QR> findAllByProcessedDateIsNull();

    boolean existsByQrCode(String qrCode);

    @Query("select qr from QR qr left join fetch qr.parsedQR r where qr.client.id = :clientId")
    @Transactional
    List<QR> findAllByClient(@Param("clientId") Long id, Pageable pageable);


    QRPart findByClientId(Long id);

}