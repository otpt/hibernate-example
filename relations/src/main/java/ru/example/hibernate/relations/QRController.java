package ru.example.hibernate.relations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.example.hibernate.relations.service.QRService;

@RestController
public class QRController {
    private final QRService QRService;

    @Autowired
    public QRController(QRService QRService) {
        this.QRService = QRService;
    }

    @GetMapping(path = "/entityGraphQuery")
    public void showEntityGraphQuery() {
        QRService.showEntityGraphQuery();
    }

    @GetMapping(path = "/JPQLQuery")
    public void showJPQLQuery() {
        QRService.showJPQLQuery();
    }

    @GetMapping(path = "/wrongPaginationOverFetch")
    public void wrongPaginationOverFetch() {
        QRService.wrongPaginationOverFetch();
    }

    @GetMapping(path = "/rightPaginationOverFetch")
    public void rightPaginationOverFetch() {
        QRService.rightPaginationOverFetch();
    }


    @GetMapping(path = "/editImmutable")
    public void editImmutable() {
        QRService.editImmutable();
    }

    @GetMapping(path = "/showProjection")
    public void showProjection() {
        QRService.showProjection();
    }
}
