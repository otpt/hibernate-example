
package ru.example.hibernate.transactions_annotation.services;

import ru.example.hibernate.entities.Singer;
import ru.example.hibernate.repos.SingerRepository;
import ru.example.hibernate.services.SingerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.net.http.HttpClient;
import java.util.List;

@Service("singerService")
@Transactional
public class SingerServiceImpl implements SingerService {

	@Autowired
	private SingerRepository singerRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Singer> findAll() {
		return singerRepository.findAll();
	}

	/*
	 * API  changed in  2.0.0.M3 findOne became findById
     * @param id
     * @return
     */
	@Override
	@Transactional(readOnly = true)
	public Singer findById(Long id) {
		return singerRepository.findById(id).get();
	}

	@Override
	public Singer save(Singer singer) {
		return singerRepository.save(singer);
	}

	@Override
	@Transactional(propagation = Propagation.NEVER)
	public long countAll() {
		return singerRepository.countAllSingers();
	}
}

