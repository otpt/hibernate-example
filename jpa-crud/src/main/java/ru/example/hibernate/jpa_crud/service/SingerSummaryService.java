package ru.example.hibernate.jpa_crud.service;

import ru.example.hibernate.jpa_crud.view.SingerSummary;

import java.util.List;

public interface SingerSummaryService {
    List<SingerSummary> findAll();
}
