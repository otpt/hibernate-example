package ru.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.example.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
