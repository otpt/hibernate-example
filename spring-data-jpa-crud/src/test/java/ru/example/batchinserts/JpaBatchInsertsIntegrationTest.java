package ru.example.batchinserts;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import ru.example.batchinserts.model.School;
import ru.example.batchinserts.model.Student;
import ru.example.boot.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class JpaBatchInsertsIntegrationTest {

    @PersistenceContext
    private EntityManager entityManager;

    private static final int BATCH_SIZE = 5;

    @Transactional
    @Test
    public void whenInsertingSingleTypeOfEntity_thenCreatesSingleBatch() {
        for (int i = 0; i < 10; i++) {
            School school = TestObjectHelper.createSchool(i);
            entityManager.persist(school);
        }
    }

    @Transactional
    @Test
    public void whenFlushingAfterBatch_ThenClearsMemory() {
        for (int i = 0; i < 10; i++) {
            if (i > 0 && i % BATCH_SIZE == 0) {
                entityManager.flush();
                entityManager.clear();
            }

            School school = TestObjectHelper.createSchool(i);
            entityManager.persist(school);
        }
    }

    @Transactional
    @Test
    public void whenThereAreMultipleEntities_ThenCreatesNewBatch() {
        for (int i = 0; i < 10; i++) {
            School school = TestObjectHelper.createSchool(i);
            entityManager.persist(school);
            Student firstStudent = TestObjectHelper.createStudent(school);
            Student secondStudent = TestObjectHelper.createStudent(school);
            entityManager.persist(firstStudent);
            entityManager.persist(secondStudent);
        }
    }

    @Transactional
    @Test
    public void whenUpdatingEntities_thenCreatesBatch() {
        for (int i = 0; i < 10; i++) {
            School school = TestObjectHelper.createSchool(i);
            entityManager.persist(school);
        }

        entityManager.flush();

        TypedQuery<School> schoolQuery = entityManager.createQuery("SELECT s from School s", School.class);
        List<School> allSchools = schoolQuery.getResultList();

        for (School school : allSchools) {
            school.setName("Updated_" + school.getName());
        }
    }

    @After
    public void tearDown() {
        entityManager.flush();
    }
}
