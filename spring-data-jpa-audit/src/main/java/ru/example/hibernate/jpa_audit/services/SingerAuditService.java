package ru.example.hibernate.jpa_audit.services;

import ru.example.hibernate.jpa_audit.entities.SingerAudit;

import java.util.List;

public interface SingerAuditService {
    List<SingerAudit> findAll();
    SingerAudit findById(Long id);
    SingerAudit save(SingerAudit singer);
}
